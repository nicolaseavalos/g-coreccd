#include <cmath>	// sqrt, pow
#include <cstdio>	// printf
#include <random>	// std::normal_distribution
#include <ctime>	// time
#include <fstream>	// std::ifstream
#include <sstream>	// std::stringstream
#include <iostream>	// std::cout

#include "holesystemcpu.h"

/* CONSTANTS */
#define kB_CONST 1.3806e-23	//[J/K] Boltzmann constant
#define q_CONST 1.602e-19	//[C] electron charge.
#define EPS2 1e-11 // softening factor
#define K_CONST 1.232722e-8 // q/(4*pi*epsi0*epsiSi)
#ifndef UMTOCM
#define UMTOCM 1e-4
#endif // UMTOCM

/* Class initialization */
HoleSystemCPU::HoleSystemCPU(simParams *_sP)
	: sP(_sP)
{
	_initialize();
}

void HoleSystemCPU::_initialize()
{
	x.resize(sP->numHoles);
	y.resize(sP->numHoles);
	z.resize(sP->numHoles);
	Ezstat.resize(sP->numHoles);
	// Edin need to initialize to 0 explicitly
	Exdin.resize(sP->numHoles, 0);
	Eydin.resize(sP->numHoles, 0);
	Ezdin.resize(sP->numHoles, 0);
	Ez.resize(sP->numHoles);
	Emag.resize(sP->numHoles);
	muHoles.resize(sP->numHoles);
	Vx.resize(sP->numHoles);
	Vy.resize(sP->numHoles);
	Vz.resize(sP->numHoles);

	gen.seed(time(NULL));
}

/* Class destruction */
HoleSystemCPU::~HoleSystemCPU()
{
	_finalize();
}

void HoleSystemCPU::_finalize()
{
	
}

/* Load initial conditions */
void HoleSystemCPU::reset(double depth)
{
	if (sP->loadFromFile){
		loadCsvFile(sP->csvname);
	}
	else {
		std::normal_distribution<float> dist1(0,sP->sig0*UMTOCM);
		std::normal_distribution<float> dist2(depth*UMTOCM,sP->sig0*UMTOCM);
		for (int n=0; n<sP->numHoles; n++) {
			x[n] = dist1(gen);
			y[n] = dist1(gen);
			z[n] = dist2(gen);
			if (z[n] > sP->thickness*UMTOCM) 
				z[n] = sP->thickness*UMTOCM;
		}
	}
	flag = false;
}

/* Load positions from .csv file */
void HoleSystemCPU::loadCsvFile(const std::string &filename)
{
	std::ifstream input;
	input.open(filename);
	int i=0; 
	float holePos[3], val;
	std::string line;

	while(getline(input, line)){
		// do not load commented lines
		if(line[0]=='#')
			continue;
		// Create a stringstream of the current line
		std::stringstream ss(line);
		int colIdx = 0;
		// Extract each float
		while(ss >> val){
			// Add the current integer to the 'colIdx' column's values vector
			holePos[colIdx] = val;

			// If the next token is a comma, ignore it and move on
			if(ss.peek() == ',') ss.ignore();

			// Increment the column index
			colIdx++;
		}
		// Copy values to cpu vectors
		x[i] = holePos[0]*UMTOCM;
		y[i] = holePos[1]*UMTOCM;
		z[i] = holePos[2]*UMTOCM;

		i++;
	}
}

/* One simulation step */
void HoleSystemCPU::update(double dt)
{
	/* Compute static electric field */
	for(int n=0; n<sP->numHoles; n++) {
		Ezstat[n] = ((z[n]*sP->slopeE) - sP->cteE);
		if(Ezstat[n]>0) {
			printf("\nERROR: Positive electric field!!! z=%f um ; EzStat=%f\n", z[n]/UMTOCM, Ezstat[n]);
		}
	}

	/* Compute Coulomb repulsion */
	if(sP->calc_Edin) {
		/* Reset values */
		for(int n=0; n<sP->numHoles; n++) {
			Exdin[n]=0;
			Eydin[n]=0;
			Ezdin[n]=0;
		}
		/* Compute */
		for(int n=0; n<sP->numHoles; n++) {
			for(int i=n+1; i<sP->numHoles; i++) {
				float rx = x[n]-x[i];
				float ry = y[n]-y[i];
				float rz = z[n]-z[i];

				float distSqr = rx*rx + ry*ry + rz*rz + EPS2 ;
				float distSixth = distSqr * distSqr * distSqr;
				float invDistCube = 1./sqrt(distSixth);
				
				float s = K_CONST * invDistCube;
				float sx = s*rx;
				float sy = s*ry;
				float sz = s*rz;

				Exdin[n] += sx;
				Exdin[i] -= sx;
				Eydin[n] += sy;
				Eydin[i] -= sy;
				Ezdin[n] += sz;
				Ezdin[i] -= sz;
			}
		}
	}

	/* Compute total electric field */
	for(int n=0; n<sP->numHoles; n++) {
		Ez[n] = Ezdin[n] + Ezstat[n];
		Emag[n] = sqrt((Exdin[n]*Exdin[n]) + 
					   (Eydin[n]*Eydin[n]) + 
					   (Ez[n]*Ez[n]));
	}

	/* Compute holes mobility */
	for(int n=0; n<sP->numHoles; n++) {
		muHoles[n] = sP->mu0 / pow(1+pow(Emag[n]/sP->den_factor, sP->exp_factor), sP->inv_exp_factor);
	}

	/* Compute holes velocity */
	for(int n=0; n<sP->numHoles; n++) {
		Vx[n] = muHoles[n]*Exdin[n];
		Vy[n] = muHoles[n]*Eydin[n];
		Vz[n] = muHoles[n]*Ez[n];
	}

	/* Compute next position */
	/* Diffusion vectors std */
	float sigD = sqrt(2*kB_CONST*sP->Temp*sP->mu0*dt/q_CONST);
	std::normal_distribution<float> dist(0,sigD);
	flag = true;
	for(int n=0; n<sP->numHoles; n++) {
		/* Update positions */
		if(z[n] >= (sP->potWellDepth*UMTOCM)) {	//Check if the holes fell in the potential well.
			x[n] += (Vx[n]*dt) + dist(gen);
			y[n] += (Vy[n]*dt) + dist(gen);
			z[n] += (Vz[n]*dt) + dist(gen);
			flag = false;
		}
		if (z[n] > sP->thickness) // check that the charges do not escape the ccd
			z[n] = sP->thickness;
	}
}

/* Check if simulation is over */
bool HoleSystemCPU::checkFlags(int iter)
{
	if (sP->max_iters == 0)
		return flag;
	return iter >= sP->max_iters;
}

/* Save positions to .csv file */
std::string get_time_string(); // defined in simEvents.cpp
void HoleSystemCPU::savePos(double time, bool final)
{
	/* Define file name */
	std::string filename;
	std::string pref;
	char c_time[32]; sprintf(c_time, "%.3lf", time);
	if (! final)
		pref = "aux_" + std::string(c_time) + "ns_" ;
	else pref = "out_";
	if (sP->loadFromFile){
		auto prefix = sP->csvname.find_last_of("/");
		auto suffix = sP->csvname.find_last_of(".");
		filename = pref + get_time_string() + sP->csvname.substr(prefix+1,suffix-prefix-1) + ".csv" ;
	}
	else filename = pref + get_time_string() + "_nH_" + std::to_string(sP->numHoles) + ".csv";
	filename = sP->outFolder + FILESEP + filename; // added output folder
	std::cout << " ==> Saving positions to " << filename << std::endl;
	/* Open file */
	std::ofstream posCSV;
	posCSV.open(filename);
	/* Print simulation info */
	sP->print(posCSV);
	/* Save results */
	posCSV << "# time = " << time << " ns" << std::endl;
	for (int i=0; i<sP->numHoles; i++){
		posCSV << x[i]/UMTOCM << "," << y[i]/UMTOCM << "," << z[i]/UMTOCM << std::endl;
	}
	/* Close file */
	posCSV.close();
}

/* Get the spread of the charges (in cm) */
double HoleSystemCPU::getSpread()
{
	double spread_aux = 0;
	for (int n=0; n<sP->numHoles; n++){
		spread_aux += x[n]*x[n];
	}
	return sqrt(spread_aux/sP->numHoles);
}