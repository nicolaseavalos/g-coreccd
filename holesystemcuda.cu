#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform.h>
#include <thrust/for_each.h>
#include <thrust/functional.h>		// thrust::plus, thrust::multplies
#include <thrust/tuple.h>			//thrust::make_tuple
#include <thrust/logical.h>     	// thrust::all_of
#include <thrust/inner_product.h>	// thrust::inner_product

/* cuRand calls wrapper */
#define CURAND_CALL(x) do { if((x)!=CURAND_STATUS_SUCCESS) { \
	printf ("Error at %s:%d\n",__FILE__,__LINE__);}} while(0)

/* CUDA calls wrapper */
#define CUDA_CALL(call)\
{ \
    const cudaError_t error = call; \
    if (error != cudaSuccess) \
    {  \
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__); \
        fprintf(stderr, "code: %d, reason: %s\n", error, \
        cudaGetErrorString(error)); \
        exit(1); \
    } \
}

#include <fstream>	// std::ifstream, std::ofstream
#include <sstream>  // std::stringstream
#include <string>	// std::string
#include <algorithm> // std::remove_if

#ifndef UMTOCM
#define UMTOCM 1e-4
#endif // UMTOCM

#define kB_CONST 1.3806e-23	//[J/K] Boltzmann constant
#define q_CONST 1.602e-19	//[C] electron charge.

#include "holesystemcuda.h"
#include "functors.h"
#include "simconfig.h"	// sP
#include "gpu_timer.h"	// gpu_timer

/* Useful structs */
struct Rawpointers {
	float *x, *y, *z;
	float *Dx, *Dy, *Dz;
	float *Exdin, *Eydin, *Ezdin;
};

/* Declaration of kernel (defined in nbody_kernel.cu) */
__global__ void calculate_field(Float3 pos, Float3 Edin, int N, int numThreads);

/* Vectores de device */
class GPUvectors {
	friend class HoleSystemCUDA;
	thrust::device_vector<float> x, y, z; // Posiciones
	thrust::device_vector<float> Ezstat, Exdin, Eydin, Ezdin, Ez, Emag; // Campo eléctrico
	thrust::device_vector<float> muHoles; // Movilidad
	thrust::device_vector<float> Vx, Vy, Vz; // Velocidad
	thrust::device_vector<float> Dx, Dy, Dz; // Difusión
	thrust::device_vector<bool> flags;    // dice si la carga llegó al pozo o no
	Rawpointers raw;
 public:
 	GPUvectors() {}
	void init(unsigned int numHoles)
	{
		x.resize(numHoles);
		y.resize(numHoles);
		z.resize(numHoles);
		Ezstat.resize(numHoles);
		// Edin need to initialize to 0 explicitly
		Exdin.resize(numHoles, 0);
		Eydin.resize(numHoles, 0);
		Ezdin.resize(numHoles, 0);
		Ez.resize(numHoles);
		Emag.resize(numHoles);
		muHoles.resize(numHoles);
		Vx.resize(numHoles);
		Vy.resize(numHoles);
		Vz.resize(numHoles);
		flags.resize(numHoles);
		Dx.resize(numHoles);
		Dy.resize(numHoles);
		Dz.resize(numHoles);
		/* raw pointers */
		raw.x = thrust::raw_pointer_cast(&x[0]);
		raw.y = thrust::raw_pointer_cast(&y[0]);
		raw.z = thrust::raw_pointer_cast(&z[0]);
		raw.Exdin = thrust::raw_pointer_cast(&Exdin[0]);
		raw.Eydin = thrust::raw_pointer_cast(&Eydin[0]);
		raw.Ezdin = thrust::raw_pointer_cast(&Ezdin[0]);
		raw.Dx = thrust::raw_pointer_cast(&Dx[0]);
		raw.Dy = thrust::raw_pointer_cast(&Dy[0]);
		raw.Dz = thrust::raw_pointer_cast(&Dz[0]);
	}
	void reset_flags()
	{
		flags.assign(flags.size(), false);
	}
};

/* Vectores de host */
class CPUvectors {
	friend class HoleSystemCUDA;
	thrust::host_vector<float> x_host;
	thrust::host_vector<float> y_host;
	thrust::host_vector<float> z_host;
  public:
  	void init(unsigned int numHoles)
  	{
  		x_host.resize(numHoles);
  		y_host.resize(numHoles);
  		z_host.resize(numHoles);
  	}
  	void assign(unsigned int i, float x, float y, float z)
  	{
  		x_host[i] = x;
  		y_host[i] = y;
  		z_host[i] = z;
  	}
};

/* Class initialization */
HoleSystemCUDA::HoleSystemCUDA(simParams *_sP)
	: sP(_sP)
{
	_initialize();
}

void HoleSystemCUDA::_initialize()
{
	gpu = new GPUvectors;
	gpu->init(sP->numHoles);

	if (sP->loadFromFile || sP->saveToCSV) {
		cpu = new CPUvectors;
		cpu->init(sP->numHoles);
	}

	/* Create pseudo-random number generator */
	CURAND_CALL(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
	/* Set seed */
	CURAND_CALL(curandSetPseudoRandomGeneratorSeed(gen, time(NULL)));;
}

/* Class destruction */
void HoleSystemCUDA::_finalize()
{
	delete gpu;
	if (sP->loadFromFile || sP->saveToCSV)
		delete cpu;
}

/* Load positions from .csv file */
void HoleSystemCUDA::loadCsvFile(const std::string& filename)
{
	std::ifstream input;
	input.open(filename);
	int i=0; 
	float holePos[3], val;
	std::string line;

	while(getline(input, line)){
		// do not load commented lines
		if(line[0]=='#')
			continue;
		// Create a stringstream of the current line
		std::stringstream ss(line);
		int colIdx = 0;
		// Extract each float
		while(ss >> val){
			// Add the current integer to the 'colIdx' column's values vector
			holePos[colIdx] = val;

			// If the next token is a comma, ignore it and move on
			if(ss.peek() == ',') ss.ignore();

			// Increment the column index
			colIdx++;
		}
		// Copy values to cpu vectors
		cpu->assign(i, holePos[0]*UMTOCM, 
					  holePos[1]*UMTOCM, 
					  holePos[2]*UMTOCM);
		i++;
	}
	/* Copy to GPU */
	gpu->x = cpu->x_host;
	gpu->y = cpu->y_host;
	gpu->z = cpu->z_host;
}

/* Save positions to .csv file */
std::string get_time_string(); // defined in useful_functions.h
void HoleSystemCUDA::savePos(double time, bool final)
{
	/* Define file name */
	std::string filename;
	std::string pref;
	char c_time[32]; sprintf(c_time, "%.3lf", time);
	if (! final)
		pref = "aux_" + std::string(c_time) + "ns_" ;
	else pref = "out_";
	if (sP->loadFromFile){
		auto prefix = sP->csvname.find_last_of("/");
		auto suffix = sP->csvname.find_last_of(".");
		filename = pref + get_time_string() + sP->csvname.substr(prefix+1,suffix-prefix-1) + ".csv" ;
	}
	else filename = pref + get_time_string() + "_nH_" + std::to_string(sP->numHoles) + ".csv";
	filename = sP->outFolder + FILESEP + filename; // added output folder
	std::cout << " ==> Saving positions to " << filename << std::endl;
	/* Open file */
	std::ofstream posCSV;
	posCSV.open(filename);
	/* Print simulation info */
	sP->print(posCSV);
	/* Copy back to host */
	cpu->x_host = gpu->x;
	cpu->y_host = gpu->y;
	cpu->z_host = gpu->z;
	/* Save results */
	posCSV << "# time = " << time << " ns" << std::endl;
	for (int i=0; i<sP->numHoles; i++){
		posCSV << cpu->x_host[i]/UMTOCM << "," << cpu->y_host[i]/UMTOCM << "," << cpu->z_host[i]/UMTOCM << std::endl;
	}
	/* Close file */
	posCSV.close();
}

/* Set initial positions */
void HoleSystemCUDA::reset(double depth)
{
	if (sP->loadFromFile){
		loadCsvFile(sP->csvname);
	}
	else {
		/* CuRand needs even number of elements */
		int even_numHoles = sP->numHoles;
		if(even_numHoles%2)
			even_numHoles+=1;
		/* Generate random floats on device */
		CURAND_CALL(curandGenerateNormal(gen, gpu->raw.x, even_numHoles, 0.0f, sP->sig0*UMTOCM));
		CURAND_CALL(curandGenerateNormal(gen, gpu->raw.y, even_numHoles, 0.0f, sP->sig0*UMTOCM));
		CURAND_CALL(curandGenerateNormal(gen, gpu->raw.z, even_numHoles, depth*UMTOCM, sP->sig0*UMTOCM));

		/* Chequear que la profundidad máxima sea thickness */
		thrust::transform(gpu->z.begin(), gpu->z.end(), gpu->z.begin(), check_max_depth(sP->thickness));
	}
	gpu->reset_flags();
}

/* One simulation step */
void HoleSystemCUDA::update(double dt)
{
	/* Calcular el campo estático Ezstat */
	thrust::transform(gpu->z.begin(), gpu->z.end(), gpu->Ezstat.begin(), get_static_E(sP->slopeE, sP->cteE));

	/* Campo eléctrico dinámico */
	if (sP->calc_Edin){
		int sharedMemSize = sP->numThreads * 3 * sizeof(float); // 3 floats for pos

		/* Grilla de threads suficientemente grande */
		dim3 nThreads(sP->numThreads);
		dim3 nBlocks((sP->numHoles + nThreads.x - 1) / nThreads.x);

		Float3 pos;
		pos.x = gpu->raw.x;
		pos.y = gpu->raw.y;
		pos.z = gpu->raw.z;

		Float3 Edin;
		Edin.x = gpu->raw.Exdin;
		Edin.y = gpu->raw.Eydin;
		Edin.z = gpu->raw.Ezdin;

		calculate_field<<< nBlocks, nThreads, sharedMemSize >>>(pos, Edin, sP->numHoles, sP->numThreads);

		CUDA_CALL(cudaDeviceSynchronize());
	}

	/* Calcular campo total Ez */
	thrust::transform(gpu->Ezstat.begin(), gpu->Ezstat.end(), gpu->Ezdin.begin(), gpu->Ez.begin(), thrust::plus<float>());

	/* módulo del campo eléctrico */
	thrust::for_each(
	  thrust::make_zip_iterator(thrust::make_tuple(gpu->Exdin.begin(), gpu->Eydin.begin(), gpu->Ez.begin(), gpu->Emag.begin())),
	  thrust::make_zip_iterator(thrust::make_tuple(gpu->Exdin.end(), gpu->Eydin.end(), gpu->Ez.end(), gpu->Emag.end())),
	  get_modulus()
	);

	/* Calcular mobilidad de los huecos */
	thrust::transform(gpu->Emag.begin(),
					  gpu->Emag.end(), 
					  gpu->muHoles.begin(), 
					  calculate_mobility(sP->mu0, sP->den_factor, sP->exp_factor, sP-> inv_exp_factor));

	/* Calcular velocidad de los huecos */
	thrust::transform(gpu->muHoles.begin(), gpu->muHoles.end(), gpu->Exdin.begin(), gpu->Vx.begin(), thrust::multiplies<float>()
	);
	thrust::transform(gpu->muHoles.begin(), gpu->muHoles.end(), gpu->Eydin.begin(), gpu->Vy.begin(), thrust::multiplies<float>()
	);
	thrust::transform(gpu->muHoles.begin(), gpu->muHoles.end(), gpu->Ez.begin(), gpu->Vz.begin(), thrust::multiplies<float>()
	);

	/* Calcular siguiente posición */
	/* Inicializar vectores de difusion aleatorios */
	double sigD = sqrt(2*kB_CONST*sP->Temp*sP->mu0*dt/q_CONST);
	/* CuRand needs even number of elements */
	int even_numHoles = sP->numHoles;
	if(even_numHoles%2)
		even_numHoles+=1;
	CURAND_CALL(curandGenerateNormal(gen, gpu->raw.Dx, even_numHoles, 0.0f, sigD));
	CURAND_CALL(curandGenerateNormal(gen, gpu->raw.Dy, even_numHoles, 0.0f, sigD));
	CURAND_CALL(curandGenerateNormal(gen, gpu->raw.Dz, even_numHoles, 0.0f, sigD));

	/* Actualizar posiciones */
	/* Primero en x & y, sólo si se tiene que actualizar */
	thrust::for_each(
	  thrust::make_zip_iterator(
	  	thrust::make_tuple(
	  		gpu->x.begin(), 
	  		gpu->Vx.begin(), 
	  		gpu->Dx.begin(), 
	  		gpu->flags.begin())),
	  thrust::make_zip_iterator(
	  	thrust::make_tuple(
	  		gpu->x.end(), 
	  		gpu->Vx.end(), 
	  		gpu->Dx.end(),
	  		gpu->flags.end())),
	  update_position(dt)
	);
	thrust::for_each(
	  thrust::make_zip_iterator(
	  	thrust::make_tuple(
	  		gpu->y.begin(), 
	  		gpu->Vy.begin(), 
	  		gpu->Dy.begin(), 
	  		gpu->flags.begin())),
	  thrust::make_zip_iterator(
	  	thrust::make_tuple(
	  		gpu->y.end(), 
	  		gpu->Vy.end(), 
	  		gpu->Dy.end(), 
	  		gpu->flags.end())),
	  update_position(dt)
	);

	/* Después en z, para determinar cuáles se tienen que actualizar la próxima */
	thrust::for_each(
	  thrust::make_zip_iterator(
	  	thrust::make_tuple(
	  		gpu->z.begin(),
	  		gpu->Vz.begin(),
	  		gpu->Dz.begin(),
	  		gpu->flags.begin())),
	  thrust::make_zip_iterator(
	  	thrust::make_tuple(
	  		gpu->z.end(),
	  		gpu->Vz.end(),
	  		gpu->Dz.end(),
	  		gpu->flags.end())),
	  update_position_z(dt, sP->potWellDepth, sP->thickness)
	  );
}

/* Check if simulation is over */
bool HoleSystemCUDA::checkFlags(int iter)
{
	if (sP->max_iters == 0)
		return thrust::all_of(gpu->flags.begin(), gpu->flags.end(), thrust::identity<bool>());
	return iter >= sP->max_iters;
}

/* Get the spread of the charges (in cm) */
double HoleSystemCUDA::getSpread()
{
	double spread_aux = thrust::inner_product(gpu->x.begin(), gpu->x.end(), gpu->x.begin(), 0.0f);
	return sqrt(spread_aux/sP->numHoles);
}

bool readnTFile(std::string fname, int *opt_nT, int numHoles)
{
	bool bFound = false;
	*opt_nT = 0;

	std::ifstream inFile;
	inFile.open(fname);
	std::string line;
	int readnH, readnT;
	while(!bFound && getline(inFile, line)) {
		/* do not load commented lines */
		if(line[0]=='#')
			continue;
		std::stringstream ss (line);
		ss >> readnH >> readnT;
		if (readnH == numHoles) {
			*opt_nT = readnT;
			bFound = true;
		}
	}

	inFile.close();
	return bFound;
}

/* Get optimal number of threads */
void HoleSystemCUDA::getOptNumThreads()
{
	/* Get device name */
	cudaDeviceProp deviceProp;
	int dev; 
	CUDA_CALL(cudaGetDevice(&dev));
	CUDA_CALL(cudaGetDeviceProperties(&deviceProp, dev));
	std::string gpuName = deviceProp.name;
	gpuName.erase(std::remove_if(gpuName.begin(), gpuName.end(), ::isspace), gpuName.end());

	/* See if optimal nT is known */
	std::string fname = "opt_nT_" + gpuName + ".dat";
	int opt_nT;
	if(readnTFile(fname, &opt_nT, sP->numHoles)) {
		std::cout << "> Getting optimal number of threads from " << fname << '\n';
		sP->numThreads = opt_nT;
	}
	/* Else find optimal nT */
	else {
		std::cout << "> Running test to get optimal number of GPU threads..." << std::endl;
		int max_nT = deviceProp.maxThreadsPerBlock;
		int warpSize = deviceProp.warpSize;
		double opt_runtime=1e9;
		/* Set initial positions */
		reset(0);
		for (int n=warpSize; n<=max_nT; n+=warpSize) {
			sP->numThreads = n;
			/* Warm-up */
			update(sP->dt);
			/* Measure time */
			gpu_timer Reloj;
			Reloj.tic();
			for (int j=0; j<2; j++){
				update(sP->dt);
			}
			double runtime = Reloj.tac();
			/* Update */
			if (opt_runtime > runtime) {
				opt_nT = n;
				opt_runtime = runtime;
			} 
		}
		/* Set optimal nT to parameters */
		sP->numThreads = opt_nT;
		std::cout << "> ... Done! Saving results to " << fname << '\n';
		/* Save */
		std::ofstream outFile;
		outFile.open(fname, std::ofstream::app);
		outFile << sP->numHoles << " " << sP->numThreads << std::endl;
		outFile.close();
	}
}

HoleSystemCUDA::~HoleSystemCUDA()
{
	_finalize();
}