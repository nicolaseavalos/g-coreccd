#ifndef __HOLESYSTEMCPU_H__
#define __HOLESYSTEMCPU_H__

#include "holesystem.h"	// HoleSystem
#include "simconfig.h"	// simParams
#include <vector>		// std::vector 
#include <random>		// std::default_random_engine

/* Main CPU class */
class HoleSystemCPU : public HoleSystem
{
  public:
  	HoleSystemCPU(simParams *_sP);
  	virtual ~HoleSystemCPU();	// OK
  	
  	virtual void update(double deltaTime);	// OK
  	virtual void reset(double depth);	// OK
  	virtual void savePos(double time, bool final); // OK
  	virtual bool checkFlags(int iter);	// OK
  	virtual double getSpread();	// OK
    virtual void getOptNumThreads() {}

  protected: //methods
  	HoleSystemCPU() {}

  	virtual void _initialize();	// OK
  	virtual void _finalize();	// OK
  	virtual void loadCsvFile(const std::string &filename);

  protected: //data
  	bool flag; // true if all charges fell into the pot well
  	std::default_random_engine gen;
  	simParams *sP;
  	std::vector<float> x, y, z; // Posiciones
	std::vector<float> Ezstat, Exdin, Eydin, Ezdin, Ez, Emag; // Campo eléctrico
	std::vector<float> muHoles; // Movilidad
	std::vector<float> Vx, Vy, Vz; // Velocidad

};

//#include "holesystemcpu_impl.h"

#endif // __HOLESYSTEMCPU_H__