OS?=win

ifeq ($(OS),win)
	HOST_COMPILER = cl
	HOST_COMPILER_OPTIONS = /EHsc
	OBJ = /Fo:
	EXE = /Fe:
	OS_FLAG = win
	RM = del
	RUN = 

else
	HOST_COMPILER = g++
	HOST_COMPILER_OPTIONS = 
	OBJ = -o 
	EXE = -o 
	RM = rm -f
	OS_FLAG =
	RUN = ./
endif

NVCC := nvcc -ccbin $(HOST_COMPILER)

LIBRARIES := -lcurand

# Target rules for gpu
all: build

build: simEvents.exe

holesystemcuda.obj:holesystemcuda.cu
	$(NVCC) -o $@ -c $< -D$(OS_FLAG)

simEvents.obj:simEvents.cpp
	$(NVCC) -o $@ -c $< -D$(OS_FLAG)

nbody_kernel.obj:nbody_kernel.cu
	$(NVCC) -o $@ -c $< -D$(OS_FLAG)

simconfig.obj:simconfig.cpp
	$(NVCC) -o $@ -c $< -D$(OS_FLAG)

simEvents.exe: holesystemcuda.obj simEvents.obj nbody_kernel.obj simconfig.obj
	$(NVCC) -o $@ $+ $(LIBRARIES) -D$(OS_FLAG)
	
run: build
	$(RUN)simEvents.exe

# Target rules for cpu

simEvents_cpu.obj:simEvents.cpp
	$(HOST_COMPILER) $(HOST_COMPILER_OPTIONS) $(OBJ)$@ -c $< -DCPU

simconfig_cpu.obj:simconfig.cpp
	$(HOST_COMPILER) $(HOST_COMPILER_OPTIONS) $(OBJ)$@ -c $< -DCPU

holesystemcpu.obj:holesystemcpu.cpp
	$(HOST_COMPILER) $(HOST_COMPILER_OPTIONS) $(OBJ)$@ -c $<

cpu: simEvents_cpu.obj simconfig_cpu.obj holesystemcpu.obj
	$(HOST_COMPILER) $(HOST_COMPILER_OPTIONS) $(EXE)simEvents_cpu.exe $+ -DCPU

cpu_run: cpu
	$(RUN)simEvents_cpu.exe

# common

clean:
	$(RM) *.exe *.obj *.o *.exp *.lib
	
clobber: clean