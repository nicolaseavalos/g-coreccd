#include <vector>	// std::vector
#include <string>	// std::string
#include <fstream>	// std::ofstream
#include <iostream> // std::cout

#include "holesystem.h"
#include "simconfig.h"

#ifndef CPU
#include "holesystemcuda.h"
#else
#include "holesystemcpu.h"
//#include <pthread.h>
#endif

/* Some useful functions */
#include "useful_functions.h"

#define UMTOCM 1e-4

/* Some global variables */
bool bPerformance = false;	// test performance?
bool bnumbodies = false;	// set numholes in command line?
std::string versionNumber = "0.2.0"; // G-CoReCCD version

/* Main simulation class */
class simEvents
{
  public:
	static void Create()
	{
		m_sim = new simEvents;
	}
	static void Destroy()
	{
		delete m_sim;
	}

	static void init(const std::string& fname1, const std::string& fname2, int numHoles)
	{
		std::cout << "\n=========================================\n"
				  << "> Initializing simEvents\n" << std::endl;
		m_sim->_init(fname1, fname2, numHoles);
	}

	static void run()
	{
		m_sim->_run();
	}

	static void testPerformance()
	{
		m_sim->_testPerformance();
	}

	//static void doOneIteration(double deltaTime)
	//{
	//	m_sim->m_simevents->update(deltaTime);
	//}

  private:
  	static simEvents *m_sim;		// this object

	HoleSystem 	*m_simevents;	  // the simulation object
  #ifndef CPU
	HoleSystemCUDA *m_simeventsCUDA; // for GPU simulation
  #else
	HoleSystemCPU *m_simeventsCPU;   // for CPU simulation
  #endif
	simParams *sP;	// the simulation parameters
	std::ofstream spreadTXT;	// file to which save spread

  	simEvents()
  		: m_simevents(0),
  	  #ifndef CPU
  		  m_simeventsCUDA(0),
  	  #else
  		  m_simeventsCPU(0),
  	  #endif
  		  sP(0)
  	{

  	}

  	~simEvents()
  	{
  	  #ifndef CPU
  		if (m_simeventsCUDA)
  			delete m_simeventsCUDA;
  	  #else
  		if (m_simeventsCPU)
  			delete m_simeventsCPU;
  	  #endif
  		if (sP)
  			delete sP;
  	}

	void _init(const std::string& confFile, const std::string& posFile, int numHoles)
	{
		sP = new simParams;
		sP->numHoles = numHoles;
		if (! bPerformance)
			sP->fill(confFile, posFile);
	  #ifndef CPU
		m_simeventsCUDA = new HoleSystemCUDA(sP);
		m_simevents = m_simeventsCUDA;
		/* Get number of threads if necessary */
		if (sP->calc_Edin && sP->numThreads == 0) {
			m_simevents->getOptNumThreads();
		}
	  #else
		m_simeventsCPU = new HoleSystemCPU(sP);
		m_simevents = m_simeventsCPU;
	  #endif
		if (! bPerformance)
			sP->print(std::cout);
		if (sP->saveSpread || sP->saveToCSV || sP->saveAux) {
			/* Create an output directory if it doesn't exist */
			/* Platform dependent!! */
			std::string command;
		  #ifdef win	// windows
			command = "if not exist \"" + sP->outFolder + "\\\" (md \"" + sP->outFolder + "\\\")";
		  #else			// linux
			command = "mkdir -p " + sP->outFolder;
		  #endif
			std::system(command.c_str());
		}
	}

	void _run()
	{
		if (sP->saveSpread) {
			/* Open file to which save spread info */
			if (sP->spreadFile == "")
				sP->spreadFile = "spread_" + get_time_string() + "_nH_" + std::to_string(sP->numHoles) + ".txt" ;
			sP->spreadFile = sP->outFolder + FILESEP + sP->spreadFile; // added to output folder
			if (! fileExists(sP->spreadFile)) {
				spreadTXT.open(sP->spreadFile);
				sP->print(spreadTXT);
				if (sP->loadFromFile)
					spreadTXT << "#file spread spread_err time time_err" << std::endl;
				else spreadTXT << "#depth spread spread_err time time_err" << std::endl;
			}
			else spreadTXT.open(sP->spreadFile, std::ofstream::app);
			std::cout << "Saving spread info to " << sP->spreadFile << std::endl;
		}

		/* Do stuff */
		if(sP->depths.size()>0) {
		//  #ifdef CPU
		//	/* Enable multithreading */
		//	pthread_t tid[sP->depths.size()];
		//	int tNum[sP->depths.size()];
		//	std::cout << "Will create " << sP->depths.size() << " threads in CPU." << std::endl;
		//  #endif
			for(int j=0; j<sP->depths.size(); j++){
			//  #ifdef CPU
			//	/* Execute threads */
			//	pthread_create(&tid[j], NULL, coreSimEvents_mt, (void*) &sP->depths[j]);
			//  #else
				std::cout << "depth = " << sP->depths[j] << std::endl;
				coreSimEvents(sP->depths[j]);
			//  #endif
			}
		//  #ifdef CPU
		//	/* Join threads */
		//	for (int j=0; j<sP->depths.size(); j++) {
		//		pthread_join(tid[j], NULL);
		//	}
		//  #endif
		}
		else{
			coreSimEvents(0);
		}
	}

	void coreSimEvents(float depth)
	{
		/* Initialize vectors */
		std::vector<float> spreads(sP->nreps, 0);	// spreads in x (um)
		std::vector<float> times(sP->nreps, 0);		// time to reach pot well (s)
		/* Repeat the simulation NE times */
		for(int p=0; p<sP->nreps; p++) {
			/* Time step initializing */
			double t=0;
			double taux=0;
			double terr=0;

			/* Reset initial positions and flags */
			m_simevents->reset(depth);

			/* Iterate */
			bool stop_iterating = false;
			int iter = 0;
			while (!stop_iterating) {
				iter++;
			//  #ifndef CPU
				std::cout << "rep " << p+1 << "; t = " << t*1e9 << " ns" << '\r' << std::flush;
			//  #endif
				/* Update hole positions */
				m_simevents->update(sP->dt);

				/* Update time step */
				double dterr = sP->dt - terr;
				t = taux + dterr;
				terr = (t-taux) - dterr;
				taux = t;

				/* Check if iterations are over */
				stop_iterating = m_simevents->checkFlags(iter);

				/* Save aux positions if needed */
				if (sP->saveAux && iter%sP->auxRate == 0){
					m_simevents->savePos(t*1e9, false);
				}
			}
			/* Save final positions to csv */
			if (sP->saveToCSV)
				m_simevents->savePos(t*1e9, true);

			/* Get final spread of this repetition */
			double spread_aux = m_simevents->getSpread();
			spreads[p] = spread_aux;
			times[p] = t;
		}

		/* Print important stuff */
		double mean_spread = mean(spreads)/(UMTOCM); // um
		double mean_time = mean(times)*1e9;			 // ns
		double err_spread = stddev(spreads)/(UMTOCM);// um
		double err_time = stddev(times)*1e9;		 // ns

		std::cout << "spread = (" << mean_spread << "+-" << err_spread << ") um; time = ("<< mean_time << "+-" << err_time << ") ns" << std::endl;

		/* Save results */
		if (sP->saveSpread) {
			if (sP->loadFromFile)
				spreadTXT << sP->csvname ;
			else spreadTXT << depth ;
			spreadTXT << " " << mean_spread << " " << err_spread << " " << mean_time << " " << err_time << std::endl;
		}
	}

	void _testPerformance()
	{
		/* Initialize stuff */
		m_simevents->reset(0);
		
		/* Warm-up */
		m_simevents->update(sP->dt);

		/* Start timer */
		cpu_timer Reloj;
		Reloj.tic();

		/* Run iterations */
		for (int i=0; i<10; i++) {
			m_simevents->update(sP->dt);
		}

		/* Stop timer */
		double runtime = Reloj.tac();
		std::cout << "> Number of charges: " << sP->numHoles << "\n> Runtime for 10 iterations: " << runtime << " ms." << std::endl;
		std::ofstream outFile;
	  #ifdef CPU
		outFile.open("cpuPerf.txt", std::ofstream::app);
	  #else
		outFile.open("gpuPerf.txt", std::ofstream::app);
	  #endif
		outFile << sP->numHoles << " " << runtime << '\n';
	}

//  #ifdef CPU
//	static void *coreSimEvents_mt(void *pArgs)
//	{
//		float *depth;
//		depth = (float*)pArgs;
//		std::cout << "depth = " << depth[0] << std::endl;
//		m_sim->coreSimEvents(depth[0]);
//		pthread_exit(NULL);
//	}
//  #endif
};

/* This little line below has made me waste 4 hours and skip a meal */
simEvents *simEvents::m_sim = 0;

/* ============== Main ============== */
int main(int argc, char const *argv[])
{
	/* Create simulation class */
	simEvents::Create();

	/* Print welcome message */
	std::cout << "\n========= Welcome to G-CoReCCD! =========\n";
	std::cout << "> Software Version: " << versionNumber << '\n';
	std::cout << "=========================================\n\n";

	/* Initialize parameters with provided arguments */
	if (checkCmdLineFlag(argc, (const char **) argv, "performance")) {
		bPerformance = true;
	}
	std::string confFile;
	if (! getCmdLineArgumentString(argc, (const char **) argv, "conf", confFile)) {
		if (! bPerformance)
			std::cout << "> No .conf file provided (use -conf=<confFile> to provide one). Will ask for user input." << std::endl;
	}
	else if (bPerformance) {
		std::cout << "> WARNING: use of -performance and -conf=<confFile> simultaneously is not supported. Will ignore .conf file." << std::endl;
	}
	std::string posFile;
	if (! getCmdLineArgumentString(argc, (const char **) argv, "pos", posFile)) {
		if (! bPerformance)
			std::cout << "> No hole positions file provided (use -pos=<posFile> to provide one)." << std::endl;
	}
	else if (bPerformance) {
		std::cout << "> WARNING: use of -performance and -pos=<posFile> simultaneously is not supported. Will ignore position file." << std::endl;
	}
	int numholes = 0;
	if (checkCmdLineFlag(argc, (const char **) argv, "numholes")) {
		bnumbodies = true;
		numholes = getCmdLineArgumentInt(argc, (const char **) argv, "numholes");
		if (numholes < 1) {
			std::cout << "> Error: \"number of holes\" specified " << numholes << " is invalid.  Value should be >= 1\n";
			exit(EXIT_SUCCESS);
		}
	}
	else if (bPerformance) {
		std::cout << "> ERROR: use of -performance needs -numholes=<numholes> flag. Exiting." << std::endl;
		exit(EXIT_SUCCESS);
	}

	simEvents::init(confFile,posFile,numholes);

	/* Run! */
	if (bPerformance) {
		simEvents::testPerformance();
	}
	else {
		/* Simulate and time */
		cpu_timer Reloj;
		Reloj.tic();
		simEvents::run();
		double runtime = Reloj.tac();
		std::cout << "> Total simulation time: " << runtime << " ms" << std::endl;
	}

	/* Cleanup */
	simEvents::Destroy();
	exit(EXIT_SUCCESS);
}