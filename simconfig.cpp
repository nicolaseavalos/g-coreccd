#include "simconfig.h"

#include <string>	// std::string
#include <iostream>	// std::cout, std::cin
#include <cstdlib>  // EXIT_SUCCESSM, EXIT_FAILURE
#include <fstream>  // std::ifstream
#include <stdexcept> // std::invalid_argument
#include <vector>	// std::vector
#include <cmath>    // pow
#include <algorithm>  // std::remove_if


/* Input file utility functions */
int count_lines(std::ifstream *input)
{
	int numlines = 0;
	std::string line;
	while (getline(*input, line)) {
		if (line[0] == '#') // líneas con comentarios se saltean
			continue;
		numlines+=1;
	}
	return numlines;
}

int getNumHolesfromFile(const std::string& filename)
{
	/* Lee archivo de entrada */
	std::ifstream input;
	input.open(filename);
	if (! input.is_open()){
		std::cout << filename << ": unable to open file" << std::endl ;
		return 0;
	}

	/* Obtiene número de cargas */
	int numHoles = count_lines(&input);
	input.close();
	return numHoles;
}

bool isConfFile(const std::string& filename)
{
	auto dotPos = filename.find_last_of(".");
	std::string extension = filename.substr(dotPos,filename.size()-dotPos);
	if (extension == ".conf")
		return true;
	return false;
}

/* User input functions */

bool yesNoQuery(const std::string& query)
{
	std::string answer;
	do {
		std::cout << query ;
		getline(std::cin, answer);
	} while ( (answer!="y") && (answer!="n") );

	if(answer=="y") return true;
	return false;
}

void numberQuery(const std::string& query, int& n)
{
	std::string s;
	do {
		try {
			std::cout << query ;
			getline(std::cin, s);
			n = stoi(s);
			break;
		}
		catch (const std::invalid_argument& ia) {
		std::cerr << "Invalid argument: " << ia.what() << '\n';
		}
	} while(true);
}

void numberQuery(const std::string& query, float& n)
{
	std::string s;
	do {
		try {
			std::cout << query ;
			getline(std::cin, s);
			n = stof(s);
			break;
		}
		catch (const std::invalid_argument& ia) {
		std::cerr << "Invalid argument: " << ia.what() << '\n';
		}
	} while(true);
}

template <class T>
void defaultQuery(const std::string& query, const T& defaultNumber, T& number)
{
	std::string s;
	do {
		try {
			std::cout << query ;
			getline(std::cin, s);
			if (s==""){
				number = defaultNumber;
				break;
			}
			number = stof(s);
			break;
		}
		catch (const std::invalid_argument& ia) {
		std::cerr << "Invalid argument: " << ia.what() << '\n';
		}
	} while(true);
}


/* simParams functions */

void simParams::fill(const std::string& _confname, const std::string& _csvname)
{
	simConfig config;
	if (_confname=="" && _csvname=="") {
		config = QUERY_ALL;
	}
	else if (_confname=="") {
		csvname = _csvname;
		config = FILL_FROM_CSV;
	}
	else if (_csvname=="") {
		confname = _confname;
		config = FILL_FROM_CONF;
	}
	else {
		csvname = _csvname;
		confname = _confname;
		config = FILL_FROM_BOTH;
	}

	switch (config)
	{
		default:
		case QUERY_ALL:
		{
			std::string s ;
			std::cout << "> Will generate random initial hole positions." << '\n';
			loadFromFile = 0;

			s = ">>> Enter number of charges: ";
			numberQuery(s, numHoles);

			int numDepths;
			s = ">>> Enter number of depths to simulate: ";
			numberQuery(s, numDepths);
			depths.resize(numDepths);
			for (size_t i=0; i<numDepths; i++ ){
				s = ">>> Enter depth nº " + std::to_string(i+1) + "(in um): ";
				numberQuery(s, depths[i]);
			}
			
			commonQuery();
			break;
		}
		case FILL_FROM_CSV:
			fillFromCSV();
			commonQuery();
			break;

		case FILL_FROM_BOTH:
			fillFromCSV();
		case FILL_FROM_CONF:
			std::ifstream confFile (confname);
			if (confFile.is_open()) {
				std::string line;
				while (getline(confFile, line)) {
					/* Remove spaces and ignore comments */
					line.erase(std::remove_if(line.begin(), line.end(), isspace),line.end());
					if(line[0] == '#' || line.empty())
						continue;
					/* Find name and value for this line */
					auto delimiterPos = line.find("=");
					auto name = line.substr(0, delimiterPos);
					auto value = line.substr(delimiterPos + 1);
					/* Assign value to corresponding name */
					if(name=="calc_Edin")
						calc_Edin = std::stoul(value);
					else if (name=="saveToCSV")
						saveToCSV = std::stoul(value);
					else if (name=="saveSpread")
						saveSpread = std::stoul(value);
					else if (name=="numHoles")
						numHoles = std::stoi(value);
					else if (name=="nreps")
						nreps = std::stoi(value);
					else if (name=="numThreads")
						numThreads = std::stoi(value);
					else if (name=="depths") {
						auto commaPos = value.find_first_of(",");
						while(commaPos != std::string::npos){
							depths.push_back(std::stof(value.substr(0,commaPos)));
							value.erase(0,commaPos+1);
							commaPos = value.find_first_of(",");
						}
					}
					else if (name=="spreadFile")
						spreadFile = value;
					else if (name=="dt")
						dt = std::stod(value);
					else if (name=="saveAux")
						saveAux = std::stoul(value);
					else if (name=="auxRate")
						auxRate = std::stoi(value);
					else if (name=="thickness")
						thickness = std::stof(value);
					else if (name=="pixelWidth")
						pixelWidth = std::stof(value);
					else if (name=="potWellDepth")
						potWellDepth = std::stof(value);
					else if (name=="cteE")
						cteE = std::stof(value);
					else if (name=="slopeE")
						slopeE = std::stof(value);
					else if (name=="Temp")
						Temp = std::stod(value);
					else if (name=="csvname") {
						if (csvname != ""){
							std::cout << "> Warning! .csv file loaded in both command line and .conf file. Will use the one set in command line." << '\n';
							continue;
						}
						csvname = value;
					}
					else if (name=="max_iters")
						max_iters = std::stoi(value);
				}
				confFile.close();
			}
			else {
				std::cout << "> Error: couldn't open config file for reading. Exiting.\n";
				exit(EXIT_FAILURE);
			}
			if (numHoles <= 0) {
				std::cout << "> Error: <numHoles> not set up in .conf file. Exiting." << std::endl;
				exit(EXIT_SUCCESS);
			}
			if (loadFromFile == 0 && depths.size()==0) {
				std::cout << "> Error: <depths> not set up in .conf file. Cannot generate random positions. Exiting." << '\n';
				exit(EXIT_SUCCESS);
			}
			
	}

	mu0=(1.31e8*pow(Temp,-2.2));
	den_factor = 1.24 * pow(Temp, 1.68);
	exp_factor = 0.46 * pow(Temp, 0.17);
	inv_exp_factor = 1./exp_factor;

	if(!loadFromFile) {
		if (numHoles < 6000)
			sig0 = 0.257*0.0171*pow(numHoles*0.003745,1.75);
		else sig0 = 2;
	}

	std::cout << '\n' << "> Simulation parameters loaded correctly!" << '\n' << std::endl;
}

void simParams::commonQuery()
{
	std::string s = ">>> Include Coulomb repulsion in calculation? (y/n): ";
	calc_Edin = yesNoQuery(s);
  #ifndef CPU
	if(calc_Edin){
		s = ">>> Enter number of GPU threads (0 to auto decide) [0]: ";
		defaultQuery(s, 0, numThreads);
	}
  #endif
	s = ">>> Enter simulation time step dt in ns [0.01]: ";
	defaultQuery(s, 0.01, dt);
	dt *= 1e-9; // ns to s

	s = ">>> Enter max number of iterations (0 for iterating until end) [0]: ";
	defaultQuery(s, 0, max_iters);
	
	s = ">>> Enter number of repetitions for each simulation [1]: ";
	defaultQuery(s, 1, nreps);

	s = ">>> Save final positions to CSV file? (y/n): ";
	saveToCSV = yesNoQuery(s);

	if (saveToCSV){
		s = ">>> Save auxiliary CSV files? (y/n): ";
		saveAux = yesNoQuery(s);

		if (saveAux){
			s = ">>> Aux CSV files every how many iterations? [100]: ";
			defaultQuery(s, 100, auxRate);
		}
	}

	s = ">>> Save spread information to txt file? (y/n): ";
	saveSpread = yesNoQuery(s);

	if (saveSpread){
		std::cout << ">>> Enter name of output spread information file (or nothing for default) [default]: " ;
		getline(std::cin, spreadFile);
	}

	/* CCD parameters query */
	s = ">>> Enter CCD thickness in um [250.0]: ";
	defaultQuery(s,250.0f,thickness);

	s = ">>> Enter pixel width in um [15.0]: ";
	defaultQuery(s,15.0f,pixelWidth);

	s = ">>> Enter potential well depth in um [0.0]: ";
	defaultQuery(s,0.0f,potWellDepth);

	std::cout << ">>> Static field calculation: Ez = a1*z - a2" << std::endl;
	s = ">>> Enter value of slope a1 in V/cm^2 [95800.0]: ";
	defaultQuery(s,95800.0f,slopeE);

	s = ">>> Enter value of constant a2 in V/cm [2900.0]: ";
	defaultQuery(s,2900.0f,cteE);

	s = ">>> Enter CCD temperature in K [140.0]: ";
	defaultQuery(s,140.,Temp);
}

void simParams::fillFromCSV()
{
	loadFromFile = true;
	numHoles = getNumHolesfromFile(csvname);
	if (numHoles == 0){
		std::cout << "> Error: Could not create initial hole positions." << std::endl;
		exit(EXIT_FAILURE);
	}
	std::cout << "> Read file " << csvname << " .\n";
	std::cout << "> Will create " << numHoles << " holes." << std::endl;
}

void simParams::print(std::ostream& output)
{
	output << "#=============================================" << '\n';
	output << "#            Simulation parameters" << '\n';
	output << "#=============================================" << '\n';
	if (loadFromFile){
		output << "# Loading charges from file " << csvname << '\n';
	}
	else{
		output << "# Generating xray data from normal distribution" << '\n';
		output << "# sig0 = " << sig0 << '\n';
		output << "# at depths " ;
		for(float depth : depths){
			output << depth << " , ";
		}
		output << "um" << std::endl;
	}
	output << "# Total number of charges: " << numHoles << '\n';
	output << "# Number of repetitions per simulation: " << nreps << '\n';
	output << "# Time step (dt): " << dt << " s" << '\n';
	if (max_iters==0)
		output << "# Iterating until all charges reach potential well" << '\n';
	else output << "# Number of iterations: " << max_iters << '\n';

	if(calc_Edin){
		output << "# Coulomb repulsion ON" << '\n';
	  #ifndef CPU
		output << "# Number of GPU threads: " << numThreads << '\n';
	  #else
		output << "# Computing in CPU\n";
	  #endif 
	}
	else{
		output << "# Coulomb repulsion OFF" << '\n';
	}
	if(saveToCSV){
		output << "# WILL create output .csv file with final positions" << '\n';
	}
	else output << "# WILL NOT create output .csv file" << '\n';
	if (saveAux)
		output << "# WILL save aux CSV files every " << auxRate << " iterations" << '\n';
	if (saveSpread)
		output << "# WILL create .txt file with spread" << '\n';
	else output << "# WILL NOT create .txt file with spread" << '\n';
	output << "#=============================================" << '\n' << "#" << '\n';

	output << "#=============================================" << '\n';
	output << "#                CCD properties" << '\n';
	output << "#=============================================" << '\n';
	output << "# Thickness: " << thickness << " um" << '\n';
	output << "# Pixel width: " << pixelWidth << " um" << '\n';
	output << "# Potential well depth: " << potWellDepth << " um" << '\n';
	output << "# Ez = a1*z - a2; a1=" << slopeE << " V/cm^2; a2=" << cteE << " V/cm\n";
	output << "# Temperature: " << Temp << " K\n";
	output << "# mu0 = " << mu0 << std::endl;
	output << "#=============================================" << '\n' << '\n';
}


/* Testing main */
#ifdef SIMCONFIG
int main(int argc, char const *argv[])
{
	simParams *sP = new simParams;
	std::string csvname, confname;

	if (argc==1){
		sP->fill("","");
	}

	else if (argc==2){
		std::string arg = argv[1];
		sP->fill(arg, "");
	}

	else if (argc==3){
		std::string arg1 = argv[1], arg2 = argv[2];
		sP->fill(arg1, arg2);
	}

	else {
		std::cout << "> Error: Only two arguments max! Exiting." << std::endl;
		exit(EXIT_SUCCESS);
	}

	sP->print(std::cout);

	delete sP;
	exit(EXIT_SUCCESS);
}
#endif //SIMCONFIG