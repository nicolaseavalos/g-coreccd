#ifndef __HOLESYSTEM_H__
#define __HOLESYSTEM_H__

#include <string> // std::string

struct Float3 {
  float *x, *y, *z;
};

class HoleSystem
{
  public:
  	virtual ~HoleSystem() {}

  	virtual void update(double deltaTime) = 0;

  	virtual void reset(double depth) = 0;

  	virtual void savePos(double time, bool final) = 0;

  	virtual bool checkFlags(int iter) = 0;

  	virtual double getSpread() = 0;

    virtual void getOptNumThreads() {}

  protected:
  	HoleSystem() {}

    virtual void loadCsvFile(const std::string &filename) = 0;
  	virtual void _initialize() = 0;
  	virtual void _finalize() = 0;
};


#endif // __HOLESYSTEM_H__