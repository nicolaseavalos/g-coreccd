/* CONSTANTES */
#define EPS2 1e-11 // softening factor
#define K_CONST 1.232722e-8 // q/(4*pi*epsi0*epsiSi)

#include "holesystem.h" // Float3

/* Calcula campo electrico entre dos cargas */
__device__ float3 bodyBodyInteraction(float3 myPos, float3 pos, float3 E)
{
	float3 r;
	// r_ij  [3 FLOPS]   
	r.x = myPos.x - pos.x;   
	r.y = myPos.y - pos.y;   
	r.z = myPos.z - pos.z;   

	// distSqr = dot(r_ij, r_ij) + EPS^2  [6 FLOPS]    
	float distSqr = r.x * r.x + r.y * r.y + r.z * r.z + EPS2;

	// invDistCube =1/distSqr^(3/2)  [4 FLOPS (2 mul, 1 sqrt, 1 inv)]    
	float distSixth = distSqr * distSqr * distSqr;
	float invDistCube = 1.0f/sqrtf(distSixth);   

	// s = k * invDistCube [1 FLOP]    
	float s = K_CONST * invDistCube;

	// a_i =  a_i + s * r_ij [6 FLOPS]   
	E.x += r.x * s;   
	E.y += r.y * s;   
	E.z += r.z * s;

	return E; 
}

/* Declaro variable en memoria compartida */
extern __shared__ float shPos[];

/* Calcula campo total E sentido por una carga en posicion myPosition */
__device__ float3 tile_calculation(float3 myPosition, float3 E, int tile, int N, int numThreads) 
{   
	/* Inicializo arrays de posiciones */
	float* shX = (float*)shPos;
	float* shY = (float*)&shPos[numThreads];
	float* shZ = (float*)&shPos[numThreads*2];

	/* Calculo la interaccion con todos los cuerpos del bloque */
	int limit = blockDim.x;
	if ((tile+1)*blockDim.x >= N)
		limit = N - tile*blockDim.x;
	for (int i = 0; i < limit; i++) {
		float3 shPos = make_float3(shX[i], shY[i], shZ[i]);
		E = bodyBodyInteraction(myPosition, shPos, E);
	}   

	return E; 
}

/* KERNEL */
__global__ void calculate_field(Float3 pos, Float3 Edin, int N, int numThreads)
{   
	/* Inicializo arrays de posiciones */
	float* shX = (float*)shPos;
	float* shY = (float*)&shPos[numThreads];
	float* shZ = (float*)&shPos[numThreads*2];
	
	/* Declaro variables de este hilo */
	float3 myPosition;
	int i, tile;
	float3 E = make_float3(0.0f, 0.0f, 0.0f);   
	int gtid = blockIdx.x * blockDim.x + threadIdx.x;
	if (gtid<N){
		myPosition = make_float3(pos.x[gtid], pos.y[gtid], pos.z[gtid]);

		/* Calculo Edin */
		for (i = 0, tile = 0; i < N; i += numThreads, tile++) {
			/* Cargo posicion de memoria global a compartida */
			int idx = tile * blockDim.x + threadIdx.x;     
			if (idx < N) {
				shX[threadIdx.x] = pos.x[idx];
				shY[threadIdx.x] = pos.y[idx];
				shZ[threadIdx.x] = pos.z[idx];
			}
			__syncthreads(); // Espero a que todos los hilos lo hagan
			/* Calculo el vector de campo */
			E = tile_calculation(myPosition, E, tile, N, numThreads);     
			__syncthreads();
		}   

		/* Guardo el resultado en memoria global */     
		Edin.x[gtid] = E.x;
		Edin.y[gtid] = E.y;
		Edin.z[gtid] = E.z;
	}
} 