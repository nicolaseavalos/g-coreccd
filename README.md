# G-CoreCCD v0.2

A GPU-based Coulomb repulsion and charge transport simulator for fully-depleted thick CCDs

## Changelog

### v0.2
- Added compatibility with Windows 10 (tested with Visual Studio 2019)

### v0.1.1
- function `getOptNumThreads` now makes two iterations of the simulation instead of only one
- fixed the errors occurred when trying to initialize odd number of charges randomly
- changed default values `calc_Edin` to 1 and `numThreads` to 0
- improved the way the `-performance` flag works

## Usage - Linux

If you have a GPU, first build with
`make`,

then run with
`./simEvents [-conf=<confFile>] [-pos=<posFile> | -numholes=<numholes>]`.


If you don't have a GPU, build for cpu:
`make cpu`,

then run with
`./simEvents_cpu [-conf=<confFile>] [-pos=<posFile> | -numholes=<numholes>]`.

## Usage - Windows

Note: to build with the `make` command, [make for Windows](https://community.chocolatey.org/packages/make) should be installed.

If you have a GPU, first build with
`make OS=win`,

then run with
`simEvents.exe [-conf=<confFile>] [-pos=<posFile> | -numholes=<numholes>]`.


If you don't have a GPU, build for cpu:
`make cpu OS=win`,

then run with
`simEvents_cpu.exe [-conf=<confFile>] [-pos=<posFile> | -numholes=<numholes>]`.

## Known bugs

* When not giving a configuration file as input, flag `-numholes=<numholes>"` is ignored.
* Simulator breaks down when passing both `pos=<posFile>` and the sentence `numholes=<numholes>` in the configuration file (technically not a bug, but should be corrected).