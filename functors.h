#ifndef __FUNCTORS_H__
#define __FUNCTORS_H__

/* Get Ezstat */
struct get_static_E
{
    float m, b;
    get_static_E(float slopeE, float cteE) : m(slopeE), b(cteE) {}
    __host__ __device__ float operator() (float z)
    {
        float EzStat = z * m - b;
        if(EzStat>0) 
            printf("\nERROR: Positive electric field!!! z=%lf um ; EzStat=%lf\n", z/UMTOCM, EzStat);
        return EzStat;
    }
};

/* Get Emag */
struct get_modulus
{
    template <typename Tuple>
    __host__ __device__ void operator() (Tuple t)
    {
        float x = thrust::get<0>( t );
        float y = thrust::get<1>( t );
        float z = thrust::get<2>( t );
        thrust::get<3>(t) = sqrt(x*x + y*y + z*z);
    }
};

/* Get muHoles */
struct calculate_mobility
{
	double a, b, c, d;
	calculate_mobility(double mu0,
					   double den_factor,
					   double exp_factor,
					   double inv_exp_factor) : a(mu0), 
											 b(den_factor),
											 c(exp_factor),
											 d(inv_exp_factor) {}

	__host__ __device__ float operator() (float z)
    {
        return a/pow(1+pow(z/b,c),d);
    }
};

/* Updates positions in x & y */
struct update_position
{
    double dt;
	update_position(double _dt) : dt(_dt) {}
    template <typename Tuple>
    __host__ __device__ void operator() (Tuple t)
    {
        if (thrust::get<3>(t)==0){
        	float V = thrust::get<1>( t );
	        float D = thrust::get<2>( t );
	        thrust::get<0>(t) += V*dt + D ;
        }
    }
};

/* Updates positions in z and checks the flag */
struct update_position_z
{
    float zW, zMax;
    double dt;
	update_position_z(
		double _dt,
		float potWellDepth,
		float thickness) : zW(potWellDepth*UMTOCM), 
						   zMax(thickness*UMTOCM), 
						   dt(_dt) {}

    template <typename Tuple>
    __host__ __device__ void operator() (Tuple t)
    {
        if (thrust::get<3>(t) == 0){
        	float V = thrust::get<1>( t ); // velocidad
        	float D = thrust::get<2>( t ); // difusión
        	thrust::get<0>(t) += V*dt + D; // actualiza posición
        	if (thrust::get<0>(t) < zW)    // revisa si llegó al pozo
        		thrust::get<3>(t) = 1;
            if (thrust::get<0>(t) > zMax)  // si se "salió" del ccd, que vuelva
                thrust::get<0>(t) = zMax;
        }
    }
};

/* Ensure that all the charges stay inside the CCD */
struct check_max_depth
{
    float zMax;
    check_max_depth(float thickness) : zMax(thickness*UMTOCM) {}

    __host__ __device__ float operator() (float z)
    {
        if (z > zMax)
            return zMax;
        return z;
    }
};

#endif // __FUNCTORS_H__