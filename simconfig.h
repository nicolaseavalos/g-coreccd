#ifndef __SIMCONFIG_H__
#define __SIMCONFIG_H__

#include <string> // std::string
#include <vector> // std::vector

#ifndef FILESEP
#ifdef win
#define FILESEP "\\"
#else
#define FILESEP "/"
#endif
#endif

enum simConfig
{
	FILL_FROM_CONF,
	FILL_FROM_CSV,
	FILL_FROM_BOTH,
	QUERY_ALL
};

/* Simulation parameters */
class simParams {
  public:
	bool calc_Edin=1,		// Compute Coulomb interaction?
		 loadFromFile=0,	// Load hole positions from file?
		 saveToCSV=0,		// Save final positions to .csv file?
		 saveSpread=0,		// Save spread data to .txt file?
		 saveAux=0;			// Save auxiliary positions?
	int numHoles=0, 		// Number of charges to simulate
		nreps=1, 			// Number of simulation repetitions
		numThreads=0,		// Number of GPU threads to use 
		auxRate=100, 		// Save aux .csv every <auxRate> iterations
		max_iters=0;		// When to stop iterating (0 = end of sim)
	float sig0;				// Initial hole spread (um)
	std::vector<float> depths;	// Initial charge mean depths
  	std::string csvname;	// name of the initial position csv file
  	std::string spreadFile; // name of the file to which save final spreads
  	std::string confname;   // name of the configuration file
	std::string outFolder="outputs";	// name of the folder with outputs
  	double dt=0.01e-9;		// time step (s)

  	/* CCD parameters */
  	float pixelWidth=15,	// um
  		  thickness=250, 	// um
  		  potWellDepth=0, 	// um
  		  cteE=2900, 		// V/cm
  		  slopeE=95800;		// V/cm^2
	double Temp=140, 		// K
		   mu0, 			// cm^2/(V s)
		   den_factor, 
		   exp_factor, 
		   inv_exp_factor;

  private:
  	void commonQuery();
  	void fillFromCSV();

  public:
	simParams() {}
	~simParams() {}
	void fill(const std::string&, const std::string&);
	void print(std::ostream&);
};

#endif // __SIMCONFIG_H__