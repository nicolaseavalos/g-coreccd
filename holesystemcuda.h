#ifndef __HOLESYSTEMCUDA_H__
#define __HOLESYSTEMCUDA_H__

#include "holesystem.h"

#include <ctime>	// time()
#include <cstdio>	// sprintf()
#include <fstream>  // std::ifstream
#include <string>   // std::string

#include <curand.h>     // curandGenerator_t

/* Some declarations */
class GPUvectors;
class CPUvectors;
class simParams;

/* Main class */
class HoleSystemCUDA : public HoleSystem
{
	public:
		HoleSystemCUDA(simParams *sP);
		virtual ~HoleSystemCUDA();
		
		virtual void update(double deltaTime);
		virtual void reset(double depth);
  		virtual void savePos(double time, bool final);
  		virtual bool checkFlags(int iter);
  		virtual double getSpread();
  		virtual void getOptNumThreads();

	protected: // methods
		HoleSystemCUDA() {}	// default constructor
		
		virtual void loadCsvFile(const std::string &filename);
		virtual void _initialize();
		virtual void _finalize();

	protected: // data
		GPUvectors *gpu;
		CPUvectors *cpu;
		simParams *sP;

		curandGenerator_t gen;
};

#endif // __HOLESYSTEMCUDA_H__