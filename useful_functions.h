#ifndef __USEFUL_FUNCTIONS_H__
#define __USEFUL_FUNCTIONS_H__

#include <cstring>		// strchr, strlen
#include <vector>		// std::vector
#include <cmath>		// sqrt
#include <ctime>		// time_t, struct tm, time, localtime, strftime
#include <fstream>		// std::ifstream
#include <string>		// std::string
#include <chrono>		// clocking

// Windows includes
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#ifndef STRNCASECMP
#define STRNCASECMP _strnicmp
#endif
// Linux includes
#else
#include <strings.h>		// strncasecmp
#ifndef STRNCASECMP
#define STRNCASECMP strncasecmp
#endif
#endif

template <class T>
T mean(const std::vector<T>& v)
{
	T accum = 0;
	for (T val : v)
		accum += val;
	return accum/v.size();
}

template <class T>
T stddev(const std::vector<T>& v)
{
	T mu = mean(v);
	T accum = 0;
	for (T val : v)
		accum += (val-mu)*(val-mu);
	return sqrt(accum/v.size());
}

std::string get_time_string()
{
	/* Get current time info */
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[20];
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer, 20, "%Y%m%dT%H%M%S", timeinfo); // 20200813T134400 
	return std::string(buffer);
}

bool fileExists(const std::string& filename)
{
	std::ifstream f(filename);
	return f.good();
}

inline int stringRemoveDelimiter(char delimiter, const char *string) {
	int string_start = 0;

	while (string[string_start] == delimiter) {
		string_start++;
	}

	if (string_start >= static_cast<int>(strlen(string) - 1)) {
		return 0;
	}

	return string_start;
}

inline bool checkCmdLineFlag(const int argc, const char **argv, const char *string_ref) {
	bool bFound = false;

	if (argc >= 1) {
		for (int i = 1; i < argc; i++) {
			int string_start = stringRemoveDelimiter('-', argv[i]);
			const char *string_argv = &argv[i][string_start];

			const char *equal_pos = strchr(string_argv, '=');
			int argv_length = static_cast<int>(equal_pos == 0 ? strlen(string_argv) : equal_pos - string_argv);

			int length = static_cast<int>(strlen(string_ref));

			if (length == argv_length && !STRNCASECMP(string_argv, string_ref, length)) {
				bFound = true;
				continue;
			}
		}
	}

	return bFound;
}

inline bool getCmdLineArgumentString(const int argc, const char **argv, const char *string_ref, std::string& string_retval) {
	bool bFound = false;

	if (argc >= 1) {
		for (int i = 1; i < argc; i++) {
			int string_start = stringRemoveDelimiter('-', argv[i]);
			char *string_argv = const_cast<char*>(&argv[i][string_start]);
			int length = static_cast<int>(strlen(string_ref));

			if (!STRNCASECMP(string_argv, string_ref, length)) {
				string_retval = std::string(&string_argv[length + 1]);
				bFound = true;
				continue;
			}
		}
	}

	if (!bFound) {
		string_retval = "";
	}

	return bFound;
}

inline int getCmdLineArgumentInt(const int argc, const char **argv, const char *string_ref) {
	bool bFound = false;
	int value = -1;

	if (argc >= 1) {
		for (int i = 1; i < argc; i++) {
			int string_start = stringRemoveDelimiter('-', argv[i]);
			const char *string_argv = &argv[i][string_start];
			int length = static_cast<int>(strlen(string_ref));

			if (!STRNCASECMP(string_argv, string_ref, length)) {
				if (length + 1 <= static_cast<int>(strlen(string_argv))) {
					int auto_inc = (string_argv[length] == '=') ? 1 : 0;
					value = atoi(&string_argv[length + auto_inc]);
				}
				else {
					value = 0;
				}

				bFound = true;
				continue;
			}
		}
	}

	if (bFound) {
		return value;
	}
	else {
		return 0;
	}
}

struct timespec diff(timespec start, timespec end)
{
	timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	}
	else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

struct cpu_timer{
	std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
	std::chrono::duration<double> diff;

	cpu_timer(){
		tic();
	}
	~cpu_timer(){}

	void tic(){
		start = std::chrono::high_resolution_clock::now();
	}
	double tac(){
		end = std::chrono::high_resolution_clock::now();
		diff = end-start;
		return diff.count()*1e3; // ms
	}
};

#endif // __USEFUL_FUNCTIONS_H__